<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:z="http://indexdata.com/zebra-2.0"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    exclude-result-prefixes="dc"
    >

    <!--
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    -->

    <xsl:output method="xml" encoding="UTF-8"/>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
