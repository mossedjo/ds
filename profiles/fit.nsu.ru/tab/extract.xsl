<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:z="http://indexdata.com/zebra-2.0"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    exclude-result-prefixes="dc"
    >

    <!--
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    -->

    <xsl:output method="xml" encoding="UTF-8"/>

    <xsl:template match="text()"/>

    <xsl:template match="/">
        <xsl:apply-templates  />
    </xsl:template>

    <xsl:template match="dc:metadata">
        <z:record z:id="{normalize-space(dc:identifier)}" type="update">
            <xsl:apply-templates />
        </z:record>
    </xsl:template>

    <!-- Bib-1 12 (Local-number) -->
    <xsl:template match="dc:identifier">
        <z:index name="Local-number:w Local-number:p Any:w">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 4 (title) -->
    <xsl:template match="dc:title">
        <z:index name="Title:w Title:p Any:w">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 1003 (author) -->
    <xsl:template match="dc:creator">
        <z:index name="Author:w Author:p Any:w Any:p">
            <xsl:value-of select="text()"/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 21 (Subject-heading) -->
    <xsl:template match="dc:subject">
        <z:index name="Subject-heading:w Subject-heading:p Any:w">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 1010 (Body-of-text) -->
    <xsl:template match="dc:description">
        <z:index name="Body-of-text:w Body-of-text:p Any:w">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 2 (Corporate-name) -->
    <xsl:template match="dc:publisher">
        <z:index name="Corporate-name:w Corporate-name:p Any:w Any:p">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 30 (date) -->
    <xsl:template match="dc:date">
        <z:index name="Date:w Date:p Any:w ">
            <xsl:value-of select="s: format-date (‘2010/11/02 11:30:10’, ‘yyyy/MM/dd HH:mm:ss’)"/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 1034 (Content-type) -->
    <xsl:template match="dc:type">
        <z:index name="Content-type:w Content-type:p Any:w">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 54 (Code-language) -->
    <xsl:template match="dc:language">
        <z:index name="Code--language:w Code--language:p Any:w">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 1 (Personal-name) -->
    <xsl:template match="dc:contributor">
        <z:index name="Personal-name:w Personal-name:p Any:w">
            <xsl:value-of select="text()"/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 1220 (format) -->
    <xsl:template match="dc:format">
        <z:index name="Format:w Format:p Any:w">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 1002 (Name) -->
    <xsl:template match="dc:source">
        <z:index name="Name:w Name:p Any:w">
            <xsl:value-of select="text()"/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 12 (Local-number) -->
    <xsl:template match="dc:relation">
        <z:index name="Local-number:w Local-number:p Any:w">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 58 (Name-geographic) -->
    <xsl:template match="dc:coverage">
        <z:index name="Name-geographic:w Name-geographic:p Any:w">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>

    <!-- Bib-1 1010 (Body-of-text) -->
    <xsl:template match="dc:rights">
        <z:index name="Body-of-text:w Body-of-text:p Any:w Any:p">
            <xsl:value-of select="."/>
        </z:index>
    </xsl:template>


</xsl:stylesheet>
